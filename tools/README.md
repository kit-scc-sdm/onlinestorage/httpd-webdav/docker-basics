# Tools

## `webdav-docker`

This is a helper script for running the most common Docker commands within the context of the httpd-webdav repositories.
Building and testing requires somewhat complicated Docker commands with several parameters in order to replicate the commands running as part of CI.
`webdav-docker` builds and runs the full Docker command by looking for the required information in the filesystem.
That means it must be run for directories within checked-out Git repositories of the httpd-webdav projects.

Symlinking this script from a checked-out version of this repository into `~/bin` or similar is the recommended way of making it available.

The following commands are available:

 * `webdav-docker build <dir>`

   Builds and tags an image from the directory `<dir>`.
   If both [jq](https://github.com/stedolan/jq) and [yq](https://github.com/mikefarah/yq) are available, `.gitlab-ci.yml` will be searched for a build job and the parameters of that build job are applied.

 * `webdav-docker run-server <dir>`

   Runs the HTTPD server in the image built by `<dir>`.
   The image must already exist and will be pulled if not available locally (standard Docker behaviour).
   The server is connected to the `httpd-webdav-pytest` Docker network with the hostname `httpd`.
   This is compatible with the `pytest-suite` command.

 * `webdav-docker pytest-suite <dir> [<pytest_param> ...]`

   Runs the pytest test suite contained in `<dir>` against the running HTTPD server started by `run-server`.
   That means the container is connected to the `httpd-webdav-pytest` network and the test suite is run against `http://httpd`.
   Any additional parameters `<pytest_param>` are passed on to pytest.

 * `webdav-docker pytest-shell <dir>`

   Starts an interactive bash shell in the pytest image in a similar way as the `test-suite` command.
   The HTTPD server started by `test-server` can be reached and both pytest and curl are available.

   ```shell
   $ webdav-docker pytest-shell .
   (docker) $ curl http://$TESTS_HTTPD_HOST/
   ```
