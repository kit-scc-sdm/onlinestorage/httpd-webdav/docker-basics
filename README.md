# Docker Basics

This repositories contains utilities for developing and testing Apache HTTPd modules with Docker.
Documentation, Dockerfiles for base images and images for common modules are included.

There is also a test suite for the WebDAV protocol.

Goals:

 * Building out-of-tree Apache modules into shared libraries.

   Building modules requires a standard set of dependencies.
   An image to base builds on (`FROM`) contains these dependencies to speed-up and standardise the build process.

 * Building Apache HTTPd images with custom modules.

   This requires a standard HTTPd server, the custom modules as shared libraries and appropriate configuration to load the modules.

 * Locally running and debugging Apache HTTPd including custom modules.

   Docker images are available for pulling or re-building locally, so that combinations of modules can be tested and debugged.

 * Testing Apache HTTPd including custom modules with automated tests.

   These tests are performed by GitLab CI against specific _Configuration Images_.

All images and processes are generally based on the official community-maintained `httpd` library image.

 * docker.io/library/httpd https://hub.docker.com/_/httpd
 * https://github.com/docker-library/httpd

## Images

Docker images are stored in the registry integrated with GitLab.
That means all images are tagged with `registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics/$IMAGE_NAME`.

Images are built by GitLab CI.

For building images locally, several `--build-arg`s are required, which are usually provided by GitLab CI:

 * `REGISTRY` is the address of the registry.
 * `REGISTRY_DIR` is the base address + path for the current repository and should be used to build images based on other images in the same repository.
 * `REGISTRY_BASICS_DIR` is the base address + path for the `docker-basics` repository and should be used to build images based on base images in that repository (such as `httpd` and `builder`).

This is an example for building an image locally where the built image is properly tagged according to its location in the repository:

```shell
$ docker build --build-arg "REGISTRY=registry.hzdr.de" --build-arg "REGISTRY_DIR=registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics" --build-arg "REGISTRY_BASICS_DIR=registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics" --tag registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics/mod-mpm-itk images/mod-mpm-itk
```

### `httpd`

A custom image containing HTTPd.
This tracks `docker.io/library/httpd` but allows to perform customisations.
Most importantly, debug packages are installed for all libraries used by HTTPd so that symbols are available when debugging.

### `builder`

"Fat" base image for building HTTPd modules.

### `pytest`

Image for running tests against HTTPd using pytest, see its [README](./images/pytest/README.md).

### `mod-*`

Module builder images:
The module is built by the instructions in the Dockerfile.
In the resulting image, the source code of the module as well as the built module (`.so`) are included.

### `debug/*`

These images are for debugging specific problems.

Images are prefixed with `YYYYMM-` representing the date of inital creation, as they are typically relevant for only a limited amount of time.

It is of interest to maintain a central version of these image and make it available to collaborators to replicate issues.

### `configurations/*`

Each _Configuration Image_ is a Docker image containing the HTTPd server, any required modules, configuration files and optionally specific, well-known files in the document root of the web server.

Such a combination is suitable for automated testing, as the response of the server for any request is well-known.

## Developing Dockerfiles

When working on Dockerfiles, keep these resources at hand:

 * https://docs.docker.com/engine/reference/builder/
 * https://docs.docker.com/develop/develop-images/multistage-build/
 * https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
 * https://github.com/hadolint/hadolint

## Chores

The software shipped in the images of this repository has been chosen with stability in mind.
Nevertheless, there are occasional maintenance chores.

 * Keeping up with the latest base images.

   Dependents of the images in this repository also have to be rebuilt.

   * httpd: Apache 2.4 (provided on Debian Bullseye)

   * pytest: Python 3.9 on Debian Bullseye

     https://devguide.python.org/

   * Debian: Debian Bullseye

 * Images used in GitLab CI

   * Docker in Docker (DinD): Docker 20.10

 * Python packages inside pytest

 * Sources of Apache HTTPd Modules

## Building and Testing in GitLab CI

This repository provides

 * How does testing work in GitLab CI (job container + service)

### GitLab CI Runner

Group runner for sdm/online-storage

 * Configured runners: docker and docker-priviledged

## Guidelines for building images

 * Every Apache module repo must publish its builder image, containing the built module

 * Every Apache module builder image must follow the naming scheme: The name of the built module with underscores replaced by dashes, e.g. `mod-mpm-itk` for Apache module `mod_mpm_itk`

 * Every Apache module repo may test its built module by publishing specific apache images with appropriate configuration test data

   * Tests in this repo target this image (service in Dockerfile)

   * Repos use shell scripts as tests with curl or specific other tools (davix)

   * When multiple (custom) modules are involved, choose the repo which makes more sense

     * Note that a rebuild/test has to be triggered once a new version of the dependent image (in the foreign repo) is published

 * Tag with latest

 * Leave /src in the builder image, but run make clean or similar

### Registry

 * Reuse tags of images as much as possible

   The registry has no clean-up policy for old tags so any tagged image is stored forever

   E.g. use branch tags instead of commit tags

## Notes to be integrated

 * Pinning strategy (specifying exact tag of an image in Dockerfile or .gitlab-ci.yml) is not clear yet

 * Overview of the build process (configurations)

 * Using in other repositories: include templates, define module builder and configurations, write tests

 * Image to directory mapping

## Licensing

Except if explicitly noted otherwise, all content in this repository is licensed under the Apache License, Version 2.0 (see LICENSE file).
Detailed authorship information is part of the Git commit data or included in the files directly.

```
Copyright 2021-2022 The Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
