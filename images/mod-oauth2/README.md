# mod_oauth2

This image builds the `mod_oauth2` module from a release source tar ball published by the official repository.
The image also builds the required `liboauth2` from the same release.

 * https://github.com/zmartzone/mod_oauth2
 * https://github.com/zmartzone/liboauth2

## Runtime dependencies

The following list has been extracted from the offical bullseye packages at [v3.2.3](https://github.com/zmartzone/mod_oauth2/releases/tag/v3.2.3) (omitted: `libc6`, `libapr*1`, `apache2*`):

```
libcurl4 (>= 7.16.2)
libhiredis0.14 (>= 0.14.0)
libjansson4 (>= 2.3 and with symbol versioning, use /packages/libjansson4.deb)
libldap-2.4-2 (>= 2.4.7)
libmemcached11
libssl1.1 (>= 1.1.0)
```

## Import build results

In addition to the HTTPD module, this image also builds supporting libraries, which must also be copied.

```Dockerfile
COPY --from=mod-oauth2 /packages/ /packages/ # Packages containing dependencies, i.e. libjansson4
COPY --from=mod-oauth2 /usr/local/lib/ /usr/local/lib/ # Custom-built libs, i.e. cjose, liboauth2, liboauth2_apache
COPY --from=mod-oauth2 /usr/local/include/ /usr/local/include/ # Development Headers (optional)
COPY --from=mod-oauth2 /usr/local/apache2/modules/ /usr/local/apache2/modules/ # HTTPD modules
```

libjansson4 and cjose come directly from the `cjose-jansson` image which this image was built against.
Hence, these two can be copied directly from `cjose-jansson` (or another image) rather than being copied from this image.
