# mod_auth_openidc

This image builds the `mod_auth_openidc` module from a release source tar ball published by the official repository.

 * https://github.com/zmartzone/mod_auth_openidc

## Runtime dependencies

The following list has been extracted from the offical bullseye package at [v2.4.12.1](https://github.com/zmartzone/mod_auth_openidc/releases/tag/v2.4.12.1) (omitted: `libc6`, `libapr*1`, `apache2*`):

```
libcurl4 (>= 7.16.2)
libhiredis0.14 (>= 0.14.0)
libjansson4 (>= 2.0.1 and with symbol versioning, use /packages/libjansson4.deb)
libpcre2-8-0 (>= 10.22)
libssl1.1 (>= 1.1.0)
```

## Import build results

In addition to the HTTPD module, this image also builds supporting libraries, which must also be copied.

```Dockerfile
COPY --from=mod-auth-openidc /packages/ /packages/ # Packages containing dependencies, i.e. libjansson4
COPY --from=mod-auth-openidc /usr/local/lib/ /usr/local/lib/ # Custom-built libs, i.e. cjose
COPY --from=mod-auth-openidc /usr/local/include/ /usr/local/include/ # Development Headers (optional)
COPY --from=mod-auth-openidc /usr/local/apache2/modules/ /usr/local/apache2/modules/ # HTTPD modules
```

libjansson4 and cjose come directly from the `cjose-jansson` image which this image was built against.
Hence, these two can be copied directly from `cjose-jansson` (or another image) rather than being copied from this image.

## OpenID Connect Provider

For testing, an OpenID Connect "Provider" is required.

[qlik/simple-oidc-provider](https://hub.docker.com/r/qlik/simple-oidc-provider/) looks like the simplest solution.
The image comes with a default set of known users which can authenticate with passwords.

Alternatives:

 * https://github.com/oauthjs/node-oauth2-server
 * https://github.com/panva/node-oidc-provider
 * Full OpenID Connect provider
   * https://www.ory.sh/hydra/docs/ requires additional service for handling authentication
   * https://www.keycloak.org/ may have a testing set-up available where users can be configured via text file
   * https://hub.docker.com/r/synapsestudios/oidc-platform/ unclear...
 * Using a library to implement mini OpenID Connect Provider
   * https://github.com/axa-group/oauth2-mock-server programmable OAuth2 provider
   * Many options...
