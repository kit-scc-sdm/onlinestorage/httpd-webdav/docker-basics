import os
import pytest
import requests
from webdav3.client import Client as WebDAVClient


class BaseURLString(str):
    def __truediv__(self, other):
        """Concatenate with other separated by a slash."""
        return BaseURLString(self + '/' + other)


@pytest.fixture(scope='session')
def httpd_base_url():
    return BaseURLString(os.environ.get('TESTS_HTTPD_URL', 'http://httpd'))

@pytest.fixture(scope='session')
def skip_tls_verification():
    return os.environ.get('TESTS_SKIP_TLS_VERIFY', 'false').lower() in ('true', '1')

@pytest.fixture(scope='function')
def raw_session(skip_tls_verification):
    session = requests.Session()

    if skip_tls_verification:
        session.verify = False
        
    return session

@pytest.fixture(scope='function')
def session(request, skip_tls_verification):
    keep_alive_marker = request.node.get_closest_marker('session_keep_alive')
    if keep_alive_marker is None:
        keep_alive = True
    else:
        keep_alive = keep_alive_marker.args[0]

    _session = requests.Session()
    if not keep_alive:
        _session.headers = {'Connection': 'close'}
    
    if skip_tls_verification:
        _session.verify = False

    return _session

@pytest.fixture(scope='function')
def non_keep_alive_session(raw_session):
    raw_session.headers = {'Connection': 'close'}
    return raw_session

@pytest.fixture(scope='function')
def webdav_client(httpd_base_url, session):
    client = WebDAVClient({
        'webdav_hostname': httpd_base_url,
    })
    client.session = session
    client.verify = session.verify
    return client

@pytest.fixture(scope='function')
def create_webdav_client(httpd_base_url, session):
    def _create_webdav_client(**kwargs):
        options = {
            'webdav_hostname': httpd_base_url,
        }
        options.update(kwargs)

        client = WebDAVClient(options)
        client.session = session
        client.verify = session.verify
        return client

    return _create_webdav_client

def pytest_configure(config):
    config.addinivalue_line(
        'markers',
        'session_keep_alive(keep_alive): configure keep-alive behaviour of session fixture.',
    )
