#!/usr/bin/env python

from setuptools import setup

setup(
    name = 'pytest-apache-httpd',
    version = '0.1.0',
    author = 'Paul Skopnik',
    author_email = 'paul.skopnik@kit.edu',
    description = 'Common pytest fixtures, hooks and helpers for Apache HTTPd testing',
    py_modules = ['pytest_apache_httpd'],
    python_requires = '>=3.9',
    install_requires = [
        'pytest>=6.2.4',
        'requests>=2.25.1',
    ],
    classifiers = [
        'Framework :: Pytest',
    ],
    entry_points = {
        'pytest11': [
            'apache-httpd = pytest_apache_httpd',
        ],
    },
)
