import pytest

@pytest.fixture()
def assert_run_pytest_passed(testdir):
    def _inner(code, test_name='test_sth'):
        # create a temporary pytest test module
        testdir.makepyfile(code)

        # run pytest with the following cmd args
        result = testdir.runpytest('-v')

        # fnmatch_lines does an assertion internally
        result.stdout.fnmatch_lines([
            f'*::{test_name} PASSED*',
        ])

        # make sure that that we get a '0' exit code for the testsuite
        assert result.ret == 0

    return _inner

def test_httpd_base_url_fixture_overwrite(assert_run_pytest_passed, monkeypatch):
    monkeypatch.setenv('TESTS_HTTPD_URL', 'http://another.httpd')
    assert_run_pytest_passed("""
        def test_sth(httpd_base_url):
            assert httpd_base_url == 'http://another.httpd'
    """)

def test_httpd_base_url_fixture(assert_run_pytest_passed):
    assert_run_pytest_passed("""
        def test_sth(httpd_base_url):
            assert httpd_base_url == 'http://httpd'
    """)

def test_skip_tls_verification_fixture(assert_run_pytest_passed, monkeypatch):
    assert_run_pytest_passed("""
        def test_sth(skip_tls_verification):
            assert skip_tls_verification == False
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'something random')
    assert_run_pytest_passed("""
        def test_sth(skip_tls_verification):
            assert skip_tls_verification == False
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'true')
    assert_run_pytest_passed("""
        def test_sth(skip_tls_verification):
            assert skip_tls_verification == True
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', '1')
    assert_run_pytest_passed("""
        def test_sth(skip_tls_verification):
            assert skip_tls_verification == True
    """)


def test_raw_session_fixture(assert_run_pytest_passed, monkeypatch):
    assert_run_pytest_passed("""
        import requests

        def test_sth(raw_session):
            assert isinstance(raw_session, requests.Session)
            assert raw_session.verify == True
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'true')
    assert_run_pytest_passed("""
        def test_sth(raw_session):
            assert raw_session.verify == False
    """)

def test_session_fixture(assert_run_pytest_passed, monkeypatch):
    assert_run_pytest_passed("""
        import requests

        def test_sth(session):
            assert isinstance(session, requests.Session)
            assert session.verify == True
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'true')
    assert_run_pytest_passed("""
        def test_sth(session):
            assert session.verify == False
    """)

    assert_run_pytest_passed("""
        import pytest
        import requests

        @pytest.mark.session_keep_alive(False)
        def test_sth(session):
            prepped = session.prepare_request(
                requests.Request('GET', 'http://example.com'),
            )
            assert prepped.headers['Connection'] == 'close'
    """)

    assert_run_pytest_passed("""
        import pytest
        import requests

        @pytest.mark.session_keep_alive(True)
        def test_sth(session):
            prepped = session.prepare_request(
                requests.Request('GET', 'http://example.com'),
            )
            assert prepped.headers['Connection'] == 'keep-alive'
    """)

def test_non_keep_alive_session_fixture(assert_run_pytest_passed, monkeypatch):
    assert_run_pytest_passed("""
        import requests

        def test_sth(non_keep_alive_session):
            assert isinstance(non_keep_alive_session, requests.Session)

            prepped = non_keep_alive_session.prepare_request(
                requests.Request('GET', 'http://example.com'),
            )
            assert prepped.headers['Connection'] == 'close'
            assert non_keep_alive_session.verify == True
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'true')
    assert_run_pytest_passed("""
        def test_sth(non_keep_alive_session):
            assert non_keep_alive_session.verify == False
    """)

def test_webdav_client_fixture(assert_run_pytest_passed, monkeypatch):
    assert_run_pytest_passed("""
        from webdav3.client import Client

        def test_sth(webdav_client, httpd_base_url, session):
            assert isinstance(webdav_client, Client)
            assert webdav_client.webdav.hostname == httpd_base_url
            assert webdav_client.session == session
            assert webdav_client.verify == session.verify
            assert webdav_client.verify == True
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'true')
    assert_run_pytest_passed("""
        def test_sth(webdav_client, session):
            assert webdav_client.verify == session.verify
            assert webdav_client.verify == False
    """)

    # Check only that keep alive can be disabled
    assert_run_pytest_passed("""
        import pytest
        import requests

        @pytest.mark.session_keep_alive(False)
        def test_sth(webdav_client):
            prepped = webdav_client.session.prepare_request(
                requests.Request('GET', 'http://example.com'),
            )
            assert prepped.headers['Connection'] == 'close'
    """)

    monkeypatch.setenv('TESTS_HTTPD_URL', 'http://another.httpd')
    assert_run_pytest_passed("""
        def test_sth(webdav_client):
            assert webdav_client.webdav.hostname == 'http://another.httpd'
    """)

def test_create_webdav_client_fixture(assert_run_pytest_passed, monkeypatch):
    assert_run_pytest_passed("""
        from webdav3.client import Client

        def test_sth(create_webdav_client, httpd_base_url, session):
            webdav_client = create_webdav_client(
                webdav_login = 'mylogin',
                webdav_password = 'mypassword',
            )
            assert isinstance(webdav_client, Client)
            assert webdav_client.webdav.login == 'mylogin'
            assert webdav_client.webdav.password == 'mypassword'
            assert webdav_client.webdav.hostname == httpd_base_url
            assert webdav_client.session == session
            assert webdav_client.verify == session.verify
            assert webdav_client.verify == True
    """)

    monkeypatch.setenv('TESTS_SKIP_TLS_VERIFY', 'true')
    assert_run_pytest_passed("""
        from webdav3.client import Client

        def test_sth(create_webdav_client, session):
            webdav_client = create_webdav_client()
            assert webdav_client.verify == session.verify
            assert webdav_client.verify == False
    """)

    # Check only that keep alive can be deactivated
    assert_run_pytest_passed("""
        import pytest
        import requests

        @pytest.mark.session_keep_alive(False)
        def test_sth(create_webdav_client):
            prepped = create_webdav_client().session.prepare_request(
                requests.Request('GET', 'http://example.com'),
            )
            assert prepped.headers['Connection'] == 'close'
    """)

    monkeypatch.setenv('TESTS_HTTPD_URL', 'http://another.httpd')
    assert_run_pytest_passed("""
        def test_sth(create_webdav_client):
            assert create_webdav_client().webdav.hostname == 'http://another.httpd'
    """)
