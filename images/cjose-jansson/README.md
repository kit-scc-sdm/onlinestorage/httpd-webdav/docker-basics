# cjose and jansson

This image builds the `cjose` library (zmartzone maintenance fork) and also contains a package (`.deb`) of the `jansson` library.

The specific version of the libjansson package provides versioned symbols in the shared library file, which are referred to by the dependents compiled against it, i.e. cjose.

There are three JSON libraries (jansson, json-c and json-glib) which export symbols of the same name and symbol versioning is required to load more than one of these libraries into a process without conflicts.
The included libjansson package is taken from Debian Bookworm.

 * https://github.com/zmartzone/cjose
 * https://github.com/akheron/jansson

## Runtime dependencies

Packages:

```
libjansson4 (>= 2.3 and with symbol versioning, use /packages/libjansson4.deb)
libssl1.1 (>= 1.1.0)
```

## Import build results

This image contains built libraries and packages.

```Dockerfile
COPY --from=cjose-jansson /packages/ /packages/ # Packages containing dependencies, i.e. libjansson4
COPY --from=cjose-jansson /usr/local/lib/ /usr/local/lib/ # Custom-built libs, i.e. cjose
COPY --from=cjose-jansson /usr/local/include/ /usr/local/include/ # Development Headers (optional)
```
