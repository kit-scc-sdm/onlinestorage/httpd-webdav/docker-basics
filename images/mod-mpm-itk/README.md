# mod_mpm_itk

This image builds the `mod_mpm_itk` module from a source tar ball published on the project website.

 * http://mpm-itk.sesse.net/

## Runtime dependencies

No runtime dependencies.

## `sleep-after-fork` variant

This variant contains a patched version of MPM ITK where the process forked by MPM ITK sleeps for 30 s right after forking.
This gives enough time to attach debugging tools to the new process, so that its behaviour during request processing can be observed.

## `libcap` variant

This variant is compiled with libcap support and requires libcap to be available at runtime.

With libcap, preforked processes run as a non-privileged user (`USER` directive from mod_unixd) but with the necessary capabilities (CAP_SETUID, CAP_SETGID, ...) to eventually change to the final user (`AssignUserID` from mod_mpm_itk) for request processing.

### Runtime dependencies

```
libcap2
```
