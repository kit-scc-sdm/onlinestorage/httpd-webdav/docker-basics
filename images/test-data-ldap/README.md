# test-data-ldap

This image builds a `test-data-ldap` server based on `osixia/openldap` with added ldap entries for testing httpd images which rely on it for authentication.

The docker image is built as `registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics/test-data-ldap:latest`.  

## Structure of data added to the LDAP server

Adding the test data, the LDAP directory looks the following:
![test-data-ldap.ldif](structure_of_test-data-ldap.svg)
(See the section below for  instructions on how to generate this graph.)
### Rules for added test data
 - UIDs and GIDs should not conflict with local ones. Therefore `UIDs` and `GIDs` have to be `>= 10000`  
 - Groups:
    - The group `cn=employees,ou=groups,dc=example,dc=org` with `GID=10000` is used as base group for all users.
    - The group `cn=admins,ou=groups,dc=example,dc=org` 
    - The two groups `projectA` and `projectB` represent two projects users can be members of. 
 - Users:
    - Naming convention: 
        - `employee-<UID>-<member of group x>(-<member of group y>-...)` if member of any groups beside employee
        - `employee-<UID>-without-project` if not member of any group besides employee
    - The password for a user is derived by `<username>password`.
        - Example for `employee-10001-in-projectA`: `employee-10001-in-projectApassword`
### Generated entries in LDAP directory
#### Created groups:
| group name | gid   | group members                                                                                                                                                 | LDAP DN                                  |
|------------|-------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------|
| employees  | 10000 | employee-10000-without-project<br>employee-10001-in-projectA<br>employee-10002-in-projectB<br>employee-10003-in-projectA-projectB<br>employee-10004-in-admins | cn=employees,ou=groups,dc=example,dc=org |
| admins     | 10001 | employee-10004-in-admins                                                                                                                                      | cn=admins,ou=groups,dc=example,dc=org    |
| projectA   | 10002 | employee-10001-in-projectA<br>employee-10003-in-projectA-projectB                                                                                             | cn=projectA,ou=groups,dc=example,dc=org  |
| projectB   | 10003 | employee-10002-in-projectB<br>employee-10003-in-projectA-projectB                                                                                             | cn=projectB,ou=groups,dc=example,dc=org  |

#### Created users:
| username                            | password                                    | UID   | primary gid | group memberships                 | LDAP DN                                                             |
|-------------------------------------|---------------------------------------------|-------|-------------|-----------------------------------|---------------------------------------------------------------------|
| employee-10000-without-project      | employee-10000-without-projectpassword      | 10000 | 10000       | employees                         | uid=employee-10000-without-project,ou=groups,dc=example,dc=org      |
| employee-10001-in-projectA          | employee-10001-in-projectApassword          | 10001 | 10000       | employees<br>projectA             | uid=employee-10001-in-projectA,ou=groups,dc=example,dc=org          |
| employee-10002-in-projectB          | employee-10002-in-projectBpassword          | 10002 | 10000       | employees<br>projectB             | uid=employee-10002-in-projectB,ou=groups,dc=example,dc=org          |
| employee-10003-in-projectA-projectB | employee-10003-in-projectA-projectBpassword | 10003 | 10000       | employees<br>projectA<br>projectB | uid=employee-10003-in-projectA-projectB,ou=groups,dc=example,dc=org |
| employee-10004-in-admins            | employee-10004-in-adminspassword            | 10004 | 10000       | employees<br>admins               | uid=employee-10004-in-admins,ou=groups,dc=example,dc=org            |

## How to start the container without TLS

To start a container without TLS, no specific set-up is required.
```shell
$ docker container run registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics/test-data-ldap
```

## How to  start the container with TLS enabled
To start the container  with enabled TLS provide `openldap.crt` `openldap.key` and `openldapCA.crt` in a folder called `openldap_certs` to be mounted into it.

```shell
$ docker container run \
		--rm \
		--volume "$(pwd)/openldap_certs:/container/service/slapd/assets/certs" \
		--env LDAP_TLS=true \
		--env LDAP_TLS_CRT_FILENAME=openldap.crt \
		--env LDAP_TLS_KEY_FILENAME=openldap.key \
		--env LDAP_TLS_CA_CRT_FILENAME=openldapCA.crt \
		registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/docker-basics/test-data-ldap
```

## How to use this LDAP image from httpd
### SSSD config
To connect an HTTPD server with LDAP through sssd use the following configurations in the domain section of the `sssd.conf` of the HTTPD:

 - To tell sssd to use LDAP:
   ```
   auth_provider = ldap
   id_provider = ldap
   chpass_provider = ldap
   ```

 - <LDAP_URL> has to point to the LDAP server (e.g. ldap://openldap:389)
   ```
   ldap_uri = <LDAP_URL>
   ldap_chpass_uri = <LDAP_URL>
   ```
 - This LDAP server hast TLS disabled per default, so use the first option.   
   If this is changed use the second option and provide the CA-Certificate:
   ```
   ldap_tls_reqcert = <allow|hard>
   ldap_tls_cacert = </path/to/ca/cert.crt>
   ldap_id_use_start_tls = <false|true>
   ```

 - Configure sssd where to look for users and groups:
   ```
   ldap_search_base = dc=example,dc=org
   ldap_user_search_base = ou=users,dc=example,dc=org
   ldap_group_search_base = ou=groups,dc=example,dc=org
   ```

 - Default values from osixia/openldap docker image:
   ```
   ldap_default_bind_dn = cn=admin,dc=example,dc=org
   ldap_default_authtok = admin
   ```

### HTTPD config
To tell the HTTPD to use the ldap for authentication add the following config to `sssd.conf`.  
 - Provide any AUTH_NAME. If previously TLS was used add `TLS` after the `AuthLDAPURL` to force STARTTLS on the connection.
   ``` 
   AuthType Basic
   AuthName <AUTH_NAME>
   AuthBasicProvider ldap

   AuthLDAPURL <LDAP_URL>/dc=example,dc=org <""|TLS>
   AuthLDAPBindDN cn=admin,dc=example,dc=org
   AuthLDAPBindPassword admin
   ```

 - Amend to match the structure on the LDAP server:  
   ```
   AuthLDAPGroupAttribute     memberUid
   AuthLDAPGroupAttributeIsDN off
   ```
- Require users to have to authenticate for all requests:

   ```
   Require valid-user
   ```

 - Require users to have specific group membership:

   ```
   <RequireAll>
       Require valid-user
       Require ldap-group cn=projectA,ou=groups,dc=example,dc=org
   </RequireAll>
   ```


 - If the LDAP server has TLS enabled, add the set of trusted CAs.
   This directive must be placed in a global configuration context (outside of `VirtualHost`).
   ```
   LDAPTrustedGlobalCert CA_BASE64 </path/to/ca-certificates.crt>
   ```

 - LDAP Caching is active by default:  
 -> See [mod_ldap](https://httpd.apache.org/docs/2.4/mod/mod_ldap.html) Dokumentation.

## How to generate a graph of an LDAP directory
Thanks to [Marcin Owsiany](https://marcin.owsiany.pl/index) for publishing the [ldif2dot](https://marcin.owsiany.pl/ldif2dot-page) Python script which was used to generate this graph.

This also requires the [dot command line tool](https://graphviz.org/doc/info/command.html) from [Graphviz](https://graphviz.org/)

The script can be downloaded [here](https://marcin.owsiany.pl/ldif2dot/ldif2dot-0.2.py)! 

Steps to generate the image:

1) To also include the passwords of users in clear text, export the complete LDAP directory for example with [phpldapadmins](https://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page) export function as `export.ldif` and save in the same directory as `ldif2dot-0.2.py`.
2) Use the following command to generate the image.
```console
$ cat export.ldif | \
      sed -E '/^version:.*/d' | \
      ldif2dot-0.2.py | \
      dot -o structure_of_test-data-ldap.svg -Nshape=box -Tsvg /dev/stdin
```


