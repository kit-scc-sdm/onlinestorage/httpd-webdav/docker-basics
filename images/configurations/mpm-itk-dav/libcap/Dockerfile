ARG REGISTRY_DIR
ARG REGISTRY_DIR_TAG_POSTFIX

FROM $REGISTRY_DIR/mod-mpm-itk:libcap${REGISTRY_DIR_TAG_POSTFIX} as mod-mpm-itk

FROM $REGISTRY_DIR/httpd:latest${REGISTRY_DIR_TAG_POSTFIX}

# Copy additional modules into the image
COPY --from=mod-mpm-itk /usr/local/apache2/modules/* /usr/local/apache2/modules/

# Install run time dependencies for additional modules
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		libcap2 \
	; \
	rm -rf /var/lib/apt/lists/*;

COPY httpd.conf /usr/local/apache2/conf/httpd.conf

RUN set -eux; \
	# Create and make world-writeable a directory for the DAVLockDB (mod_dav_fs)
	mkdir -p /usr/local/apache2/var/dav; \
	touch /usr/local/apache2/var/dav/lockdb /usr/local/apache2/var/dav/lockdb.dir /usr/local/apache2/var/dav/lockdb.pag; \
	chown -R root:root /usr/local/apache2/var/dav; \
	chmod -R a+rwX /usr/local/apache2/var/dav; \
	# Create users and groups
	groupadd some-group; \
	useradd -u 2021 -G some-group my-user; \
	useradd -u 2022 -G some-group other-user; \
	# Create a user-accessible directory and file in htdocs
	mkdir /usr/local/apache2/htdocs/my-user; \
	echo "accessible data" > /usr/local/apache2/htdocs/my-user/content.txt; \
	chown -R my-user:my-user /usr/local/apache2/htdocs/my-user; \
	chmod -R o-rwx /usr/local/apache2/htdocs/my-user; \
	# Create a read-accessible directory and file in htdocs
	mkdir /usr/local/apache2/htdocs/my-user-read-accessible; \
	echo "read-accessible data" > /usr/local/apache2/htdocs/my-user-read-accessible/content.txt; \
	chown -R my-user:my-user /usr/local/apache2/htdocs/my-user-read-accessible; \
	chmod -R ug-w,o-rwx /usr/local/apache2/htdocs/my-user-read-accessible; \
	# Create a supplementary-group-readable directory (with SETGID) and file in htdocs
	mkdir /usr/local/apache2/htdocs/group-accessible; \
	echo "accessible data" > /usr/local/apache2/htdocs/group-accessible/content.txt; \
	chown -R other-user:some-group /usr/local/apache2/htdocs/group-accessible; \
	chmod -R o-rwx,ug+rwX,g+s /usr/local/apache2/htdocs/group-accessible; \
	# Create a not-user-accessible directory and file in htdocs
	mkdir /usr/local/apache2/htdocs/other-user; \
	echo "not-accessible data" > /usr/local/apache2/htdocs/other-user/content.txt; \
	chown -R other-user:other-user /usr/local/apache2/htdocs/other-user; \
	chmod -R o-rwx /usr/local/apache2/htdocs/other-user;
