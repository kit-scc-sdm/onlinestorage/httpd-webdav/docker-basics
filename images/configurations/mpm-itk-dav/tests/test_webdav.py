import pytest
from webdav_suite import DirInfo, FileInfo, import_tests, PermissionSet, set_up_suite

# MPM ITK evaluates the authenticated user after each request and potentially
# performs an irreversible setuid. So we make sure that the requests library
# uses each connection only for one request.
pytestmark = pytest.mark.session_keep_alive(False)

files = [
    FileInfo(
        '/my-user/content.txt',
        'accessible data\n',
        permissions = PermissionSet(read=True, write=True),
    ),
    FileInfo(
        '/my-user-read-accessible/content.txt',
        'read-accessible data\n',
        permissions = PermissionSet(read=True, write=False),
    ),
    FileInfo(
        '/group-accessible/content.txt',
        'accessible data\n',
        permissions = PermissionSet(read=True, write=True),
    ),
    FileInfo(
        '/other-user/content.txt',
        'not-accessible data\n',
        permissions = PermissionSet(read=False, write=False),
    ),
]

directories = [
    DirInfo(
        '/my-user',
        ['content.txt'],
        permissions = PermissionSet(read=True, write=True),
    ),
    DirInfo(
        '/my-user-read-accessible',
        ['content.txt'],
        permissions = PermissionSet(read=True, write=False),
    ),
    DirInfo(
        '/group-accessible',
        ['content.txt'],
        permissions = PermissionSet(read=True, write=True),
    ),
    DirInfo(
        '/other-user',
        ['content.txt'],
        permissions = PermissionSet(read=False, write=False),
    ),
]

set_up_suite(globals(), files, directories)
import_tests(globals(), exclude_tests=['test_propfind_not_accessible'])
import_tests(
    globals(), only_tests=['test_propfind_not_accessible'],
    decorate=pytest.mark.xfail(reason='mod-mpmitk-setuid#2 PROPFIND Privilege Escalation'),
)
