from pathlib import PurePosixPath
import pytest
from webdav3.exceptions import (
    MethodNotSupported,
    RemoteParentNotFound,
    RemoteResourceNotFound,
    ResponseErrorCode,
)

from .suite import (
    FileInfo,
    find_child_file,
)

def assert_remote_file_equals(webdav_client, file_info):
    r = webdav_client.session.get(webdav_client.get_url(file_info.path))
    assert r.status_code == 200
    assert r.text == file_info.data
    assert int(r.headers['Content-Length']) == file_info.length
    assert r.headers['Content-Type'] == file_info.content_type

def assert_remote_path_not_existent(webdav_client, path):
    r = webdav_client.session.get(webdav_client.get_url(path))
    assert r.status_code == 404

def test_head_accessible(httpd_base_url, session, file_with_read_perm):
    r = session.head(httpd_base_url / file_with_read_perm.path)
    assert r.status_code == 200
    assert int(r.headers['Content-Length']) == file_with_read_perm.length
    assert r.headers['Content-Type'] == file_with_read_perm.content_type
    assert 'Last-Modified' in r.headers
    assert 'ETag' in r.headers

def test_get_accessible(httpd_base_url, session, file_with_read_perm, remote_file_with_read_perm):
    # MAYB: Split into static and remote fixture dependent assertion containing test functions
    r = session.get(httpd_base_url / file_with_read_perm.path)
    assert r.status_code == 200
    assert r.text == file_with_read_perm.data
    assert int(r.headers['Content-Length']) == file_with_read_perm.length
    assert r.headers['Content-Type'] == file_with_read_perm.content_type
    assert r.headers['Last-Modified'] == remote_file_with_read_perm.last_modified
    assert r.headers['ETag'] == remote_file_with_read_perm.etag

def test_head_not_accessible(httpd_base_url, session, file_with_no_perm):
    r = session.head(httpd_base_url / file_with_no_perm.path)
    assert r.status_code == 403
    for content_header in ('ETag', 'Last-Modified'):
        assert content_header not in r.headers

def test_get_not_accessible(httpd_base_url, session, file_with_no_perm):
    r = session.get(httpd_base_url / file_with_no_perm.path)
    assert r.status_code == 403
    for content_header in ('ETag', 'Last-Modified'):
        assert content_header not in r.headers

def test_head_not_existent(httpd_base_url, session, not_existent_path_info):
    r = session.head(httpd_base_url / not_existent_path_info.path)
    assert r.status_code == 404

def test_get_not_existent(httpd_base_url, session, not_existent_path_info):
    r = session.get(httpd_base_url / not_existent_path_info.path)
    assert r.status_code == 404

def test_propfind_accessible(webdav_client, remote_file_with_read_perm):
    info = webdav_client.info(remote_file_with_read_perm.path)
    assert int(info['size']) == remote_file_with_read_perm.length
    assert info['modified'] == remote_file_with_read_perm.last_modified
    assert info['etag'] == remote_file_with_read_perm.etag
    # TODO: Content-Type
    # TODO: Other fields?

def test_propfind_not_accessible(httpd_base_url, session, file_with_no_perm):
    r = session.request('PROPFIND', httpd_base_url / file_with_no_perm.path)
    assert r.status_code == 403

def test_propfind_not_existent(httpd_base_url, session, not_existent_path_info):
    r = session.request('PROPFIND', httpd_base_url / not_existent_path_info.path)
    assert r.status_code == 404

def test_propfind_free(webdav_client, not_existent_path_info):
    # This is Apache WebDAV specific
    with pytest.raises(MethodNotSupported):
        webdav_client.free()

def test_put_accessible(webdav_client, file_with_read_write_perm):
    data = file_with_read_write_perm.data + 'more content!\n'

    r = webdav_client.session.put(
        webdav_client.get_url(file_with_read_write_perm.path),
        data = data,
    )

    assert r.status_code == 204
    assert_remote_file_equals(webdav_client, FileInfo(
        file_with_read_write_perm.path,
        data,
    ))

def test_put_not_accessible(webdav_client, file_with_no_write_perm):
    data = file_with_no_write_perm.data + 'more content!\n'

    r = webdav_client.session.put(
        webdav_client.get_url(file_with_no_write_perm.path),
        data = data,
    )

    assert r.status_code == 403

def test_put_into_col_accessible(webdav_client, dir_with_read_write_perm):
    data = 'original content!\n'
    file_name = 'test_put_into_col.txt'
    while file_name in dir_with_read_write_perm.content:
        file_name = 'prefix_' + file_name
    path = str(PurePosixPath(dir_with_read_write_perm.path) / file_name)

    r = webdav_client.session.put(
        webdav_client.get_url(path),
        data = data,
    )

    assert r.status_code == 201
    assert_remote_file_equals(webdav_client, FileInfo(
        path,
        data,
    ))

def test_put_into_col_not_accessible(webdav_client, dir_with_no_write_perm):
    data = 'original content!\n'
    file_name = 'test_file_put_into_col.txt'
    while file_name in dir_with_no_write_perm.content:
        file_name = 'test_' + file_name
    path = str(PurePosixPath(dir_with_no_write_perm.path) / file_name)

    r = webdav_client.session.put(
        webdav_client.get_url(path),
        data = data,
    )

    assert r.status_code == 403

@pytest.mark.xfail(reason='production-config#40 HTTPD returns 403 instead of 409')
def test_put_into_col_not_existent(webdav_client, not_existent_path_info):
    data = 'original content!\n'
    file_name = 'test_file_put_into_col.txt'
    path = str(PurePosixPath(not_existent_path_info.path) / file_name)

    r = webdav_client.session.put(
        webdav_client.get_url(path),
        data = data,
    )

    assert r.status_code == 409

@pytest.mark.skip('not implemented')
def test_delete_accessible(webdav_client, dir_with_read_write_perm):
    r = webdav_client.session.delete(webdav_client.get_url(dir_with_read_write_perm.path))

    assert r.status_code == 201

@pytest.mark.skip('not implemented')
def test_delete_not_accessible(webdav_client, dir_with_no_write_perm):
    r = webdav_client.session.delete(webdav_client.get_url(dir_with_no_write_perm.path))

    assert r.status_code == 403

@pytest.mark.skip('not implemented')
def test_delete_not_existent():
    pass

def test_copy_accessible(webdav_client, file_with_read_perm, dir_with_read_write_perm):
    from_path = file_with_read_perm.path

    to_name = 'test_copy.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    webdav_client.copy(from_path, to_path)

    assert_remote_file_equals(webdav_client, file_with_read_perm)
    assert_remote_file_equals(webdav_client, FileInfo(
        to_path,
        file_with_read_perm.data,
    ))

def test_copy_not_accessible(webdav_client, file_with_no_perm, dir_with_read_write_perm):
    from_path = file_with_no_perm.path

    to_name = 'test_copy.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    with pytest.raises(RemoteResourceNotFound):
        webdav_client.copy(from_path, to_path)

    assert_remote_path_not_existent(webdav_client, to_path)

def test_copy_to_not_accessible(webdav_client, file_with_read_perm, file_with_no_write_perm):
    from_path = file_with_read_perm.path

    to_path = file_with_no_write_perm.path

    with pytest.raises((RemoteResourceNotFound, RemoteParentNotFound, ResponseErrorCode)) as exc_info:
        webdav_client.copy(from_path, to_path)

    assert exc_info.type is not ResponseErrorCode or exc_info.value.code == 403

    r = webdav_client.session.request(
        'COPY',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code == 403

def test_copy_not_existent(webdav_client, not_existent_path_info, dir_with_read_write_perm):
    from_path = not_existent_path_info.path

    to_name = 'test_copy.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    with pytest.raises(RemoteResourceNotFound):
        webdav_client.copy(from_path, to_path)

    assert_remote_path_not_existent(webdav_client, to_path)

def test_copy_from_not_accessible_col(webdav_client, dir_with_no_perm, dir_with_read_write_perm, all_file_infos):
    file_info = find_child_file(dir_with_no_perm, all_file_infos)
    if file_info is None:
        pytest.skip(f'No file child known for directory {dir_with_no_perm.path}')
    from_path = file_info.path

    to_name = 'test_copy.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    with pytest.raises(RemoteResourceNotFound):
        webdav_client.copy(from_path, to_path)

    r = webdav_client.session.request(
        'COPY',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code in (403, 500)

    assert_remote_path_not_existent(webdav_client, to_path)

def test_copy_to_not_accessible_col(webdav_client, file_with_read_perm, dir_with_no_write_perm, all_file_infos):
    from_path = file_with_read_perm.path

    to_name = 'test_copy.txt'
    while to_name in dir_with_no_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_no_write_perm.path) / to_name)

    with pytest.raises((RemoteParentNotFound, ResponseErrorCode)) as exc_info:
        webdav_client.copy(from_path, to_path)

    # HTTPD returns a 403 if the destination cannot be searched (missing x bit)
    # or 500 if the destination can be searched and the operation on the fs
    # failed, e.g. due to permission error (missing w bit).
    assert exc_info.type is not ResponseErrorCode or exc_info.value.code in (403, 500)

    # TODO: Submit patch to HTTPD to map any permission error to 403

    r = webdav_client.session.request(
        'COPY',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code in (403, 500)

def test_copy_to_not_existent_col(webdav_client, file_with_read_perm, not_existent_path_info):
    from_path = file_with_read_perm.path

    to_name = 'test_copy.txt'
    to_path = str(PurePosixPath(not_existent_path_info.path) / to_name)

    with pytest.raises((RemoteParentNotFound, ResponseErrorCode)) as exc_info:
        webdav_client.copy(from_path, to_path)

    assert exc_info.type is not ResponseErrorCode or exc_info.value.code == 409

    r = webdav_client.session.request(
        'COPY',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code == 409

def test_move_accessible(webdav_client, dir_with_read_write_perm, all_file_infos):
    file_info = find_child_file(dir_with_read_write_perm, all_file_infos)
    if file_info is None:
        pytest.skip(f'No file child known for directory {dir_with_read_write_perm.path}')
    from_path = file_info.path

    to_name = 'test_move.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    webdav_client.move(from_path, to_path)

    assert_remote_path_not_existent(webdav_client, from_path)
    assert_remote_file_equals(webdav_client, FileInfo(
        to_path,
        file_info.data,
    ))

    # test_move_not_accessible and test_move_to_not_accessible are not implemented.
    # move is attempted by HTTPD first via a rename and if this fails via a copy and delete.
    # A rename works even on non-accessible files, if the containing directory is modifiable.
    # Hence, it is difficult to use fixtures to properly write the aforementioned two test
    # functions and they are not implemented in this suite.

def test_move_not_existent(webdav_client, not_existent_path_info, dir_with_read_write_perm):
    from_path = not_existent_path_info.path

    to_name = 'test_move.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    with pytest.raises(RemoteResourceNotFound):
        webdav_client.move(from_path, to_path)

    assert_remote_path_not_existent(webdav_client, to_path)

def test_move_from_not_accessible_col(webdav_client, dir_with_no_perm, dir_with_read_write_perm, all_file_infos):
    file_info = find_child_file(dir_with_no_perm, all_file_infos)
    if file_info is None:
        pytest.skip(f'No file child known for directory {dir_with_no_perm.path}')
    from_path = file_info.path

    to_name = 'test_move.txt'
    while to_name in dir_with_read_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_read_write_perm.path) / to_name)

    with pytest.raises(RemoteResourceNotFound):
        webdav_client.move(from_path, to_path)

    r = webdav_client.session.request(
        'MOVE',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code in (403, 500)

    assert_remote_path_not_existent(webdav_client, to_path)

def test_move_to_not_accessible_col(webdav_client, file_with_read_perm, dir_with_no_write_perm, all_file_infos):
    from_path = file_with_read_perm.path

    to_name = 'test_move.txt'
    while to_name in dir_with_no_write_perm.content:
        to_name = 'prefix_' + to_name
    to_path = str(PurePosixPath(dir_with_no_write_perm.path) / to_name)

    with pytest.raises((RemoteParentNotFound, ResponseErrorCode)) as exc_info:
        webdav_client.move(from_path, to_path)

    # HTTPD returns a 403 if the destination cannot be searched (missing x bit)
    # or 500 if the destination can be searched and the operation on the fs
    # failed, e.g. due to permission error (missing w bit).
    assert exc_info.type is not ResponseErrorCode or exc_info.value.code in (403, 500)

    # TODO: Submit patch to HTTPD to map any permission error to 403

    r = webdav_client.session.request(
        'MOVE',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code in (403, 500)

def test_move_to_not_existent_col(webdav_client, file_with_read_perm, not_existent_path_info):
    from_path = file_with_read_perm.path

    to_name = 'test_move.txt'
    to_path = str(PurePosixPath(not_existent_path_info.path) / to_name)

    with pytest.raises((RemoteParentNotFound, ResponseErrorCode)) as exc_info:
        webdav_client.move(from_path, to_path)

    assert exc_info.type is not ResponseErrorCode or exc_info.value.code in (409, 500)

    # TODO: Submit patch to HTTPD to map any permission error to 403

    r = webdav_client.session.request(
        'MOVE',
        webdav_client.get_url(from_path),
        headers = {'Destination': webdav_client.get_url(to_path)},
    )
    assert r.status_code in (409, 500)

@pytest.mark.skip('not implemented')
def test_lock_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_lock_not_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_lock_not_existent():
    pass

@pytest.mark.skip('not implemented')
def test_proppatch_accessible():
    from webdav3.client import WebDavXmlUtils
    from webdav3.urn import Urn

    before_val = webdav_client.get_property(remote_file_with_read_perm.path, {'name': 'getlastmodified', 'namespace': 'DAV:'})

    urn = Urn(remote_file_with_read_perm.path)

    #webdav_client.set_property(remote_file_with_read_perm.path, {'name': 'getlastmodified', 'namespace': 'DAV:', 'value': 'Thu, 20 Jun 2021 11:43:42 GMT'})
    data = WebDavXmlUtils.create_set_property_batch_request_content([{'name': 'getlastmodified', 'namespace': 'DAV:', 'value': 'Thu, 20 Jun 2021 11:43:42 GMT'}])
    ret = webdav_client.execute_request(action='set_property', path=urn.quote(), data=data)

    specific_resp = WebDavXmlUtils.extract_response_for_path(ret.content, webdav_client.get_full_path(urn), webdav_client.webdav.hostname)

    print(ret.text)
    print(specific_resp)
    assert specific_resp == None # Fails, so stdout is presented to the user

    after_val = webdav_client.get_property(remote_file_with_read_perm.path, {'name': 'getlastmodified', 'namespace': 'DAV:'})
    assert before_val == remote_file_with_read_perm.last_modified
    assert before_val != after_val

@pytest.mark.skip('not implemented')
def test_proppatch_not_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_proppatch_not_existent():
    pass

def test_propfind_col_accessible(webdav_client, dir_with_read_perm):
    child_resources = webdav_client.list(remote_path=dir_with_read_perm.path)

    assert set(child_resources) == set(dir_with_read_perm.content)

def test_propfind_col_not_accessible(webdav_client, dir_with_no_perm):
    r = webdav_client.session.request(
        'PROPFIND', webdav_client.get_url(dir_with_no_perm.path),
        headers = { 'Depth': '1' },
    )
    assert r.status_code == 403

@pytest.mark.skip('not implemented')
def test_propfind_col_not_existent():
    pass

@pytest.mark.skip('not implemented')
def test_mkcol_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_mkcol_not_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_mkcol_not_existent():
    pass

@pytest.mark.skip('not implemented')
def test_post_col_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_post_col_not_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_post_col_not_existent():
    pass

@pytest.mark.skip('not implemented')
def test_delete_col_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_delete_col_not_accessible():
    pass

@pytest.mark.skip('not implemented')
def test_delete_col_not_existent():
    pass

__all__ = [
    'test_head_accessible',
    'test_get_accessible',
    'test_head_not_accessible',
    'test_get_not_accessible',
    'test_head_not_existent',
    'test_get_not_existent',
    'test_propfind_accessible',
    'test_propfind_not_accessible',
    'test_propfind_not_existent',
    'test_propfind_free',
    'test_put_accessible',
    'test_put_not_accessible',
    'test_put_not_accessible',
    'test_put_into_col_accessible',
    'test_put_into_col_not_accessible',
    'test_put_into_col_not_accessible',
    'test_put_into_col_not_existent',
    'test_delete_accessible',
    'test_delete_not_accessible',
    'test_delete_not_existent',
    'test_copy_accessible',
    'test_copy_not_accessible',
    'test_copy_to_not_accessible',
    'test_copy_not_existent',
    'test_copy_from_not_accessible_col',
    'test_copy_to_not_accessible_col',
    'test_copy_to_not_existent_col',
    'test_move_accessible',
    'test_move_not_existent',
    'test_move_from_not_accessible_col',
    'test_move_to_not_accessible_col',
    'test_move_to_not_existent_col',
    'test_lock_accessible',
    'test_lock_not_accessible',
    'test_lock_not_existent',
    'test_proppatch_accessible',
    'test_proppatch_not_accessible',
    'test_proppatch_not_existent',
    'test_propfind_col_accessible',
    'test_propfind_col_not_accessible',
    'test_propfind_col_not_existent',
    'test_mkcol_accessible',
    'test_mkcol_not_accessible',
    'test_mkcol_not_existent',
    'test_post_col_accessible',
    'test_post_col_not_accessible',
    'test_post_col_not_existent',
    'test_delete_col_accessible',
    'test_delete_col_not_accessible',
    'test_delete_col_not_existent',
]
