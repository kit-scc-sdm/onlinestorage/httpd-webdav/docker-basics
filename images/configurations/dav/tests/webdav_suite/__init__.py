from . import suite, webdav_tests
from .suite import *
from .webdav_tests import *

"""
WebDAV Test Suite

The WebDAV Test Suite provides a set of tests for WebDAV server implementations.


WebDAV is specified primarily in
[RFC 4918](https://datatracker.ietf.org/doc/html/rfc4918).
For all defined HTTP methods, tests exist in this suite, although they may not be implemented.

Using the Test Suite

For executing the tests, a pytest test module (a file `test_*.py`) has to be created and the suite be installed there.
This test module is the discovery and execution context for a particular instantiation of the test suite.

The suite must be set up by calling set_up_suite(globals()) in the test module.
All tests which should be executed have to be imported into the test module's name space.
This can be done with the import_tests(globals()) functions, which supports advanced filtering and decorator installation.
Alternatively, a wildcard import of the webdav suite package will also work.

Modifications to the test suite are done in test module by defining functions of the same name.

The test module is also a good place to define further tests for WebDAV servers, as the fixtures in this suite simplify much.

Test Function Naming Scheme

test_METHOD_DETAILS..._TARGET...

Resource Info Fixtures

{file,dir}_with_*_perm fixtures
Which kind of files are required by a test function is declared through the name of the requested fixtures.

For simplicity, the following limitations exist: There is no way to specify
that a file can be written to only (with_write_perm), as not being able to
check the outcome of an operation is of limited use in tests. Subsequently,
there is no way to specify that a file has write but no read permission
(with_write_only_perm). The "executable" permission is not represented at
all.

Server-side state restoration

Automatic clean up of mutations in the server-side state is provided by the reset_mutated fixture.
It works by inspecting all resources (files, dirs) requested by a test function with write permission and ensuring that
these match their FileInfo/DirInfo information after the test function has completed.
For this to work, the proper resource info fixtures have to be requested.
Any misuse of fixtures, for example writing successfully to a resource requested with a _with_read_perm, will lead to
unexpected state for test functions running thereafter.

The all_file_infos/all_dir_infos fixtures must also be used only for reading and not for writing.

Furthermore, not everything can be restored fully.
If this causes problems with test functions a) extend the reset_mutated method b) define a custom fixture performing the additional clean up
c) clean up directly in the test function.

Because the requests.Session is shared between fixtures and test functions,
the restoration is performed as the same user who performed the mutation.
This should always succeed from a permission perspective because for all
basic cases, the linux permission model allows a user to perform a restoring
operation if the user is allowed to perform the mutating operation.


Restoration is not triggered when a mutating operation unexpectedly succeeds because these cases
are not detected by the mechanism described above.

Authentication

Can optionally be specified statically.
Installed on the requests.Session instance yielded by session.
Provided by the session_auth fixture, which is autoused.
Additional authentication schemes can be implemented by overwriting the session_auth fixture.
Copy its standard implementation for getting started and then extend.

To Do:

 * The import-test-functions approach breaks rich assertion diffs in pytest
 * Naming scheme for not-accessible test functions
"""

def import_tests(
    module_namespace,
    only_tests = None,
    exclude_tests = set(),
    only_methods = None,
    exclude_methods = set(),
    predicate_f = lambda name: True,
    decorate = None,
):
    tests_to_import = set(webdav_tests.__all__)

    if only_tests is not None:
        tests_to_import.intersection_update(only_tests)
    tests_to_import.difference_update(exclude_tests)

    if only_methods is not None:
        tests_to_import = set(filter(
            lambda name: any(
                name.removeprefix('test_').startswith(method.lower() + '_')
                for method in only_methods
            ),
            tests_to_import,
        ))
    tests_to_import = set(filter(
        lambda name: all(
            not name.removeprefix('test_').startswith(method.lower() + '_')
            for method in exclude_methods
        ),
        tests_to_import,
    ))

    tests_to_import = set(filter(predicate_f, tests_to_import))

    for name in tests_to_import:
        func = getattr(webdav_tests, name)
        if decorate is not None:
            func = decorate(func)
        module_namespace[name] = func

__all__ = [
    'import_tests',
] + suite.__all__ + webdav_tests.__all__
