from dataclasses import dataclass
from pathlib import PurePosixPath
import pytest
import traceback
from typing import Any, List


@dataclass(frozen=True)
class BasicAuth:
    username: str
    password: str


@dataclass(frozen=True)
class TokenAuth:
    token: str


@dataclass(frozen=True)
class PermissionSet:
    read: bool = True
    write: bool = True


@dataclass(frozen=True)
class FileInfo:
    path: str
    data: str
    content_type: str = 'text/plain'
    auth: Any = None
    permissions: PermissionSet = PermissionSet()

    @property
    def length(self):
        return len(self.data)


@dataclass(frozen=True)
class RemoteFileInfo:
    path: str
    data: str
    content_type: str
    etag: str
    last_modified: str
    auth: Any = None
    permissions: PermissionSet = PermissionSet()

    @property
    def length(self):
        return len(self.data)


@dataclass(frozen=True)
class DirInfo:
    path: str
    content: List[str]
    auth: Any = None
    permissions: PermissionSet = PermissionSet()

    @property
    def content_paths(self):
        return [str(PurePosixPath('/') / self.path / self.file) for file in self.content]


def set_up_suite(module_namespace, files, directories, not_existent_paths=[]):
    if len(not_existent_paths) == 0:
        not_existent_paths = [
            FileInfo('/not-existent', ''),
            FileInfo('/not-existent/content.txt', ''),
        ]

    def _install_function(f):
        module_namespace[f.__name__] = f

    @_install_function
    @pytest.fixture(scope='session')
    def all_file_infos():
        return files

    @_install_function
    @pytest.fixture(scope='session')
    def all_dir_infos():
        return directories

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda f: f.permissions.read and f.permissions.write,
        files,
    ))
    def file_with_read_write_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda f: f.permissions.read,
        files,
    ))
    def file_with_read_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda f: f.permissions.read and not f.permissions.write,
        files,
    ))
    def file_with_read_only_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda f: not f.permissions.write,
        files,
    ))
    def file_with_no_write_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda f: not f.permissions.read and not f.permissions.write,
        files,
    ))
    def file_with_no_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda d: d.permissions.read and d.permissions.write,
        directories,
    ))
    def dir_with_read_write_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda d: d.permissions.read,
        directories,
    ))
    def dir_with_read_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda d: d.permissions.read and not d.permissions.write,
        directories,
    ))
    def dir_with_read_only_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda d: not d.permissions.write,
        directories,
    ))
    def dir_with_no_write_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=filter(
        lambda d: not d.permissions.read and not d.permissions.write,
        directories,
    ))
    def dir_with_no_perm(request):
        return request.param

    @_install_function
    @pytest.fixture(scope='session', params=not_existent_paths)
    def not_existent_path_info(request):
        return request.param

    _install_function(remote_file_with_read_write_perm)
    _install_function(remote_file_with_read_perm)
    _install_function(reset_mutated)
    _install_function(single_auth_info)
    _install_function(session_auth)

@pytest.fixture(scope='function')
def single_auth_info(request):
    def _get_auth(fixture_name):
        name = fixture_name # shorthand
        if not (
            name.startswith('file_with_') and name.endswith('_perm')
            or
            name.startswith('dir_with_') and name.endswith('_perm')
        ):
            return None

        return request.getfixturevalue(name).auth

    # remote_file_with_ fixtures request the normal file_with_ fixture, so
    # these are also covered here.

    auths = [
        auth for auth in
        (_get_auth(name) for name in request.fixturenames)
        if auth is not None
    ]

    if len(auths) == 0:
        return None

    # MAYB: Does None auth work in combination with any other auth?
    #       Yes, if None corresponds to public, no otherwise.

    auth = auths[0]
    for other_auth in auths:
        if other_auth != auth:
            pytest.skip(f'Mismatching auth infos: {auth!r} is not equal to {other_auth!r}')
            return

    return auth

def built_in_session_auth(session, auth_info):
    if auth_info is None:
        return True
    elif isinstance(auth_info, BasicAuth):
        session.auth = (auth_info.username, auth_info.password)
        return True
    elif isinstance(auth_info, TokenAuth):
        session.headers['Authorization'] = f'Bearer {auth_info.token}'
        return True

    return False

@pytest.fixture(scope='function', autouse=True)
def session_auth(session, single_auth_info):
    # TODO: Ordering of fixture execution, other fixtures require this to have run?
    if not built_in_session_auth(session, single_auth_info):
        raise Exception(f'Unsupported auth mechanism: {single_auth_info!r}')

def _restore_file(webdav_client, file_info):
    put_file = False
    r = webdav_client.session.get(webdav_client.get_url(file_info.path))
    if r.status_code == 404:
        put_file = True
    elif r.status_code == 200:
        if r.text != file_info.data:
            put_file = True
    else:
        raise Exception(
            f'Unexpected status code when fetching (GET) file: {r.status_code}',
        )

    if put_file:
        r = webdav_client.session.put(
            webdav_client.get_url(file_info.path),
            data = file_info.data,
        )
        if r.status_code < 200 or r.status_code >= 300:
            raise Exception(
                f'Unexpected status code while restoring (PUT) file content: {r.status_code}',
            )

def _restore_dir(webdav_client, dir_info, all_file_infos):
    children = webdav_client.list(remote_path=dir_info.path)

    for child in set(children).difference(dir_info.content):
        # Surplus children
        path = str(PurePosixPath(dir_info.path) / child)
        r = webdav_client.session.delete(webdav_client.get_url(path))
        if r.status_code < 200 or r.status_code >= 300:
            raise Exception(
                f'Unexpected status code while removing (DELETE) file: {r.status_code}',
            )

    for child in set(dir_info.content).difference(children):
        # Missing children
        path = str(PurePosixPath(dir_info.path) / child)

        file_info = None
        for candidate_file in all_file_infos:
            if candidate_file.path == path:
                file_info = candidate_file
                break

        if file_info is None:
            # TODO: Check all_dir_infos and recurse
            raise Exception(f'No information for missing file found: {path}')
        if not (file_info.permissions.write and file_info.permissions.read):
            raise Exception('Missing file has non-restorable permissions (no read or no write)')

        r = webdav_client.session.put(
            webdav_client.get_url(file_info.path),
            data = file_info.data,
        )
        if r.status_code < 200 or r.status_code >= 300:
            raise Exception(
                f'Unexpected status code while restoring (PUT) file: {r.status_code}',
            )

@pytest.fixture(scope='function', autouse=True)
def reset_mutated(request, webdav_client, all_file_infos):
    def _has_write_perm(prefix, fixture_name):
        name = fixture_name # shorthand
        if not (name.startswith(f'{prefix}_with_') and name.endswith('_perm')):
            return False

        return name.removeprefix(f'{prefix}_with_').removesuffix('_perm') == 'read_write'

    # remote_file_with_ fixtures request the normal file_with_ fixture, so
    # these are also covered here.

    file_infos = [
        request.getfixturevalue(name)
        for name in request.fixturenames if _has_write_perm('file', name)
    ]
    dir_infos = [
        request.getfixturevalue(name)
        for name in request.fixturenames if _has_write_perm('dir', name)
    ]

    yield

    try:
        for file_info in file_infos:
            _restore_file(webdav_client, file_info)
        for dir_info in dir_infos:
            _restore_dir(webdav_client, dir_info, all_file_infos)
    except Exception:
        # This is not recoverable: Any future test may fail because the server-side state is no
        # longer what is described by the resource info fixtures.
        pytest.exit(
            'Exception occurred during server-side state restoration: ' +
            traceback.format_exc()
        )

@pytest.fixture(scope='function')
def remote_file_with_read_write_perm(httpd_base_url, session, file_with_read_write_perm):
    r = session.get(httpd_base_url / file_with_read_write_perm.path)
    return RemoteFileInfo(
        file_with_read_write_perm.path,
        r.text,
        r.headers['Content-Type'],
        r.headers['ETag'],
        r.headers['Last-Modified'],
        permissions = file_with_read_perm.permissions,
        auth = file_with_read_perm.auth,
    )

@pytest.fixture(scope='function')
def remote_file_with_read_perm(httpd_base_url, session, file_with_read_perm):
    r = session.get(httpd_base_url / file_with_read_perm.path)
    return RemoteFileInfo(
        file_with_read_perm.path,
        r.text,
        r.headers['Content-Type'],
        r.headers['ETag'],
        r.headers['Last-Modified'],
        permissions = file_with_read_perm.permissions,
        auth = file_with_read_perm.auth,
    )

def find_child_file(dir_info, all_file_infos):
    for candidate_name in dir_info.content:
        candidate_path = str(PurePosixPath(dir_info.path) / candidate_name)
        for candidate_file in all_file_infos:
            if (
                candidate_file.path == candidate_path
                and candidate_file.auth == dir_info.auth
            ):
                return candidate_file
    return None

__all__ = [
    'BasicAuth',
    'DirInfo',
    'FileInfo',
    'PermissionSet',
    'set_up_suite',
    'TokenAuth',
]
