import pytest
from webdav3.exceptions import MethodNotSupported

# Import the suite of WebDAV tests, automatically expanded with the fixtures defined here.
from webdav_suite import *

# Data To Do
# Accessible directory with non-accessible file inside (-rw)
# Inaccessible directory with accessible file inside?

files = [
    FileInfo(
        '/accessible/content.txt',
        'accessible data\n',
        permissions = PermissionSet(read=True, write=True),
    ),
    FileInfo(
        '/read-accessible/content.txt',
        'read-accessible data\n',
        permissions = PermissionSet(read=True, write=False),
    ),
    FileInfo(
        '/group-accessible/content.txt',
        'accessible data\n',
        permissions = PermissionSet(read=True, write=True),
    ),
    FileInfo(
        '/not-accessible/content.txt',
        'not-accessible data\n',
        permissions = PermissionSet(read=False, write=False),
    ),
]

# X bit is not represented for directories, as this makes things even more complicated.
directories = [
    DirInfo(
        '/accessible',
        ['content.txt'],
        permissions = PermissionSet(read=True, write=True),
    ),
    DirInfo(
        '/read-accessible',
        ['content.txt'],
        permissions = PermissionSet(read=True, write=False),
    ),
    DirInfo(
        '/group-accessible',
        ['content.txt'],
        permissions = PermissionSet(read=True, write=True),
    ),
    DirInfo(
        '/not-accessible',
        ['content.txt'],
        permissions = PermissionSet(read=False, write=False),
    ),
]

set_up_suite(globals(), files, directories)
