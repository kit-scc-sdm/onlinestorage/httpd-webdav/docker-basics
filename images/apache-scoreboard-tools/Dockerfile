ARG REGISTRY_DIR
ARG REGISTRY_DIR_TAG_POSTFIX

FROM $REGISTRY_DIR/builder:latest${REGISTRY_DIR_TAG_POSTFIX}

ENV APACHE_SCOREBOARD_TOOLS_REF="master"

RUN set -eux; \
	git clone --bare https://github.com/bodgit/apache-scoreboard-tools.git /repo.git; \
	mkdir /src; \
	git archive --remote /repo.git --format tar "$APACHE_SCOREBOARD_TOOLS_REF" | tar -x -C /src; \
	rm -rf /repo.git;

RUN set -eux; \
	savedAptMark="$(apt-mark showmanual)"; \
	apt-get install -y --no-install-recommends \
		libevent-dev \
	; \
	( \
		cd /src; \
		make; \
		# install binaries into /usr/local/bin \
		install -d /usr/local/bin/; \
		install -m 755 check_apache /usr/local/bin/; \
		install -m 755 collectd_apache /usr/local/bin/; \
		make clean; \
	); \
	apt-mark auto '.*' > /dev/null; \
	[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false;
