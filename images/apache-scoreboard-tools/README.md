# apache-scoreboard-tools

This image builds tools for monitoring the [scoreboard](https://httpd.apache.org/docs/2.4/mod/mpm_common.html#scoreboardfile) used by HTTPD to coordinate with child processes.
Directly reading the file as a shared memory region enables out-of-band (no HTTP requests required) monitoring of the HTTPD instance.
The tools are built from the `master` branch of the upstream repository.

 * https://github.com/bodgit/apache-scoreboard-tools

## Binaries

The `check_apache` and `collectd_apache` binaries are available in `/usr/local/bin/`.

## Runtime dependencies

```
libevent-2.1-7
```

`libevent-2.1-7` is only required for running the `collectd_apache` binary.
