# MPM ITK and DAV Debug Image

This image allows debugging of MPM ITK (mod_mpm_itk) and DAV (mod_dav) in combination .

The image contains a patched version of MPM ITK where the process forked by MPM ITK sleeps for 30 s right after forking.
This gives enough time to attach debugging tools to the new process, so that its behaviour during request processing can be observed.

## Usage

Starting and stopping.

```shell
$ docker container run -d --name httpd-debug --cap-add CAP_SYS_ADMIN --cap-add CAP_SYS_PTRACE -p 9080:80 build-ci-i.lsdf.kit.edu:5000/apache-httpd-docker/debug/202104-mpm-itk-dav httpd-foreground -X
# Shutdown and remove after finishing debugging
$ docker stop httpd-debug && docker rm httpd-debug
```

 * `CAP_SYS_ADMIN` is required so that seccomp can be executed by MPM ITK to add a filter on the setuid syscall for allow only a limited UID range.
 * `CAP_SYS_PTRACE` is required to attach to running processes with debugging tools.
 * `httpd-debug` is the name of the container.
 * `httpd-foreground` is the default command of the httpd image, `-X` disables forking in the prefork MPM so that HTTPd runs in single-threaded mode. However, this does not affect the forking behaviour of MPM ITK.

Attaching with strace or gdb.

```shell
(host-terminal-1) $ docker exec -it httpd-debug bash
(host-terminal-2) $ curl -X PROPFIND http://localhost:9080/theirs/content.txt
# Look for the PID of the new process
(docker-exec) $ ps -ef
# Either: strace to log syscall performed by the process
(docker-exec) $ strace -p <PID>
# Or: gdb to debug the program by steping through lines and function calls
(docker-exec) $ gdb -p <PID>
```
